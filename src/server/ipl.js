
// 1. Number of matches played per year for all the years in IPL.
function matchesPlayedPerYear(matches) {
    let result = {};
    for (let element of matches) {
        if (result[element.season]) {
            result[element.season]++;
        } else {
            result[element.season] = 1;
        }
    }
    return result;
}


// 2. Number of matches won per team per year in IPL.
function matchesWonPerTeamPerYear(matches) {
    let result = {};
    //To get season in result.
    for (let element of matches) {
        if (result[element.season]) {
            result[element.season] = {}
        } else {
            result[element.season] = {}
        }
    }
    //To assign total wins according to year.
    Object.entries(result).map(entry => {
        let year = entry[0];
        result[year] = collectTotalWins(matches, year);
    })
    return result;
    //To get total wins per team per year.
    function collectTotalWins(matches, year) {
        let result = {}
        for (let key of matches) {
            if (key.season === year) {
                if (result[key.winner]) {
                    result[key.winner]++;
                } else {
                    result[key.winner] = 1;
                }
            }
        }
        return result;
    }
}


// 3. Extra runs conceded per team in the year 2016
function extraRunsConcededIn2016(matches, deliveries) {
    let matchIdOf2016 = []
    let result = {}
    //To get season id of 2016.
    for (let element of matches) {
        if (element.season === '2016') {
            matchIdOf2016.push(element.id);
        }
    }

    //To get extra runs of bowling team in each match.
    for (let element of deliveries) {
        if (matchIdOf2016[element.match_id]) {
            if (result[element.bowling_team]) {
                result[element.bowling_team] += parseInt(element.extra_runs);
            } else {
                result[element.bowling_team] = parseInt(element.extra_runs);
            }
        }
    }
    return result;
}


// 4. Top 10 economical bowlers in the year 2015
function top10EconomicalBowlerIn2015(matches, deliveries) {
    let matchIdOf2015 = [];
    for (let element of matches) {
        if (element.season === '2015') {
            matchIdOf2015.push(element.id);
        }
    }

    //getting bowler name along with balls bowled by them.
    let bowlerName = {}
    for (let row of deliveries) {
        if (matchIdOf2015.includes(row.match_id)) {
            if (bowlerName[row.bowler]) {
                bowlerName[row.bowler] += 1;
            } else {
                bowlerName[row.bowler] = 1;

            }
        }
    }

    let runsGivenBybowler = {}
    for (let row of deliveries) {
        if (matchIdOf2015.includes(row.match_id)) {
            if (runsGivenBybowler[row.bowler]) {
                runsGivenBybowler[row.bowler] += parseInt(row.total_runs);
            } else {
                runsGivenBybowler[row.bowler] = parseInt(row.total_runs);
            }
        }
    }

    //calculating economy rate.
    let economicalBowler = {}
    for (let val in bowlerName) {
        economicalBowler[val] = parseFloat(((runsGivenBybowler[val] / (bowlerName[val] / 6)))).toFixed(3);
    }
    //console.log(economicalBowler)
    const sortedEconomicalBowler = Object.entries(economicalBowler).sort((a, b) => a[1] - b[1]).slice(0, 10);
    
    return sortedEconomicalBowler;

}

module.exports = { matchesPlayedPerYear, matchesWonPerTeamPerYear, extraRunsConcededIn2016, top10EconomicalBowlerIn2015 };


