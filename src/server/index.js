const fs = require('fs');
const csv = require("csvtojson");
const solutionData = require('./ipl');


csv()
    .fromFile('./src/data/matches.csv')
    .then((matches) => {

        csv()
            .fromFile('./src/data/deliveries.csv')
            .then((deliveries) => {

                let matchesPlayedPerYear = JSON.stringify(solutionData.matchesPlayedPerYear(matches));
                fs.writeFileSync('./src/public/output/matchesPlayedPerYear.json', matchesPlayedPerYear, (error) => {
                    if (error) {
                        console.log(error)
                    }
                })

                let matchesWonPerTeamPerYear = JSON.stringify(solutionData.matchesWonPerTeamPerYear(matches));
                fs.writeFileSync('./src/public/output/matchesWonPerTeamPerYear.json', matchesWonPerTeamPerYear, (error) => {
                    if (error) {
                        console.log(error)
                    }
                })

                let extraRunsConcededIn2016 = JSON.stringify(solutionData.extraRunsConcededIn2016(matches, deliveries));
                fs.writeFileSync('./src/public/output/extraRunsConcededIn2016.json', extraRunsConcededIn2016, (error) => {
                    if (error) {
                        console.log(error)
                    }
                })

                let top10EconomicalBowlerIn2015 = JSON.stringify(solutionData.top10EconomicalBowlerIn2015(matches, deliveries));
                fs.writeFileSync('./src/public/output/top10EconomicalBowlerIn2015.json', top10EconomicalBowlerIn2015, (error) => {
                    if (error) {
                        console.log(error)
                    }
                })
            })
    })













